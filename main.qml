import QtQuick 2.8
import QtQuick.Controls 2.1

ApplicationWindow {
    property bool needCatch: false
    property var  shortcut: Settings.get("shortcut", { key: 81, modifiers: Qt.ControlModifier})

    visible: true
    width: 640
    height: 480
    title: qsTr("Shortcut catcher")

    //--------------------------------------------------------------------------
    Row {
        anchors.centerIn: parent
        spacing: 10

        Label {
            id: label

            text: shortcutText(shortcut)
            anchors.verticalCenter: parent.verticalCenter
        }

        Button {
            id: button

            text: qsTr("Catch shortcut")
            anchors.verticalCenter: parent.verticalCenter
            onClicked: {
                needCatch = true;
            }
        }

        focus: true
        Keys.onPressed: {
            if (needCatch) {
                setShortcut(event);
                timerForCatchTimeout.restart();
            } else if (shortcut && event.modifiers === shortcut.modifiers && event.key === shortcut.key) {
                Qt.quit();
            }
        }
    }

    //--------------------------------------------------------------------------
    Timer {
        id: timerForCatchTimeout

        interval: 500
        onTriggered: {
            needCatch = false;
        }
    }

    //--------------------------------------------------------------------------
    function setShortcut(event) {
        shortcut = {
            key: event.key,
            modifiers: event.modifiers
        };
        Settings.set ("shortcut", shortcut);

        label.text = shortcutText(shortcut);
    } // function setShortcut(event) {

    //--------------------------------------------------------------------------
    function shortcutText(shortcut) {
        var text = "";
        var withModifier = false;

        if (shortcut.modifiers & Qt.ControlModifier) {
            text = "Ctrl";
            withModifier = true;
        }
        if (shortcut.modifiers & Qt.AltModifier) {
            if (withModifier) {
                text += " + Alt";
            } else {
                text = "Alt";
            }
            withModifier = true;
        }

        if (shortcut.modifiers & Qt.ShiftModifier) {
            if (withModifier) {
                text += " + Shift";
            } else {
                text = "Shift";
            }
            withModifier = true;
        }

        var keyText;
        switch (shortcut.key) {
            case Qt.Key_Tab: keyText = "Tab"; break;
            case Qt.Key_Left: keyText = "Left"; break;
            case Qt.Key_Right: keyText = "Right"; break;
            case Qt.Key_Up: keyText = "Up"; break;
            case Qt.Key_PageUp: keyText = "PageUp"; break;
            case Qt.Key_PageDown: keyText = "PageDown"; break;
            case Qt.Key_End: keyText = "End"; break;
            case Qt.Key_Home: keyText = "Home"; break;
            case Qt.Key_Escape: keyText = "Esc"; break;
            case Qt.Key_Enter: keyText = "Enter"; break;
            case Qt.Key_Return: keyText = "Enter"; break;
            case Qt.Key_Space: keyText = "Space"; break;
            case Qt.Key_Insert: keyText = "Insert"; break;
            case Qt.Key_Delete: keyText = "Delete"; break;
            case Qt.Key_Backspace: keyText = "Backspace"; break;
            case Qt.Key_F1: keyText = "F1"; break;
            case Qt.Key_F2: keyText = "F2"; break;
            case Qt.Key_F3: keyText = "F3"; break;
            case Qt.Key_F4: keyText = "F4"; break;
            case Qt.Key_F5: keyText = "F5"; break;
            case Qt.Key_F6: keyText = "F6"; break;
            case Qt.Key_F7: keyText = "F7"; break;
            case Qt.Key_F8: keyText = "F8"; break;
            case Qt.Key_F9: keyText = "F9"; break;
            case Qt.Key_F10: keyText = "F10"; break;
            case Qt.Key_F11: keyText = "F11"; break;
            case Qt.Key_F12: keyText = "F12"; break;

            default:
                if (shortcut.key < 6000) { // Not special key
                    keyText = String.fromCharCode(shortcut.key).toLocaleLowerCase();
                }
        } // switch (event.key) {

        if (keyText) {
            if (withModifier) {
                text += " + " + keyText;
            } else {
                text += keyText;
            }
        }

        return text;
    } // function shortcutText(shortcut) {
}
